
# Context

* This repository is a demontration about possibilities offered by Legal Link Data in Europe

* This is based on a fork of a d3sparql librairie (https://github.com/ktym/d3sparql).
 * The fork is here (https://github.com/florent-andre/d3sparql) , add a usefull function. This fork will disapear when the pull query merged.

# Clone the repository and get a local installation 

* the d3sparql librairy is for now added from a submodule, so for cloning the projet and get the dependency do : 
```
git clone --recursive git@git.framasoft.org:OpenLawFr/lldd.git
```
