#!/bin/bash

fileName="released/$1.sparql"

if [ ! -f $fileName ]; then
    echo "Error : First parameter have to be a filename (without extension .sparql) in the 'released' folder"
    exit
fi

curl -X POST http://ld.openlaw.fr/bigdata/sparql --data-urlencode 'query@'$fileName -H 'Accept:application/sparql-results+json' > "../cache/$1.json"


